#!/usr/bin/env bash

set -ex

# ----------------------------------------------------------------------------------------------------------------------
# Simulate basic image generation
# ----------------------------------------------------------------------------------------------------------------------

OUTPUT_DIRNAME='output_1'
rm -rf $PWD/functional_tests/${OUTPUT_DIRNAME} || true
mkdir -p $PWD/functional_tests/${OUTPUT_DIRNAME}
PYTHONPATH=$PWD:$PYTHONPATH $PWD/heatmap_photo/run_job.py $PWD/functional_tests/params/run_params_example1.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_1/someID.zip
