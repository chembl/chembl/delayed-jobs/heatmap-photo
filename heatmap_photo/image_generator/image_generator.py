"""
Module that generates an image that represents a heatmap
"""
import json
import math
import os
import sys
import random

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

from farmhash import FarmHash128  # pylint: disable=no-name-in-module
import numpy as np
from elasticsearch.helpers import scan

from heatmap_photo.standardisation import entities
from heatmap_photo import es_connection


def generate_image(generation_params, server_conn, output_dir):
    """
    Generates an image that represents the heatmap as described in the parameters
    :param generation_params: parameters for the generation of the image, including the heatmap description
    :param server_conn: connection to the delayed jobs server
    :param output_dir: directory where to sabe the image
    :return: the path were the image was created and a dict for statistics
    """

    print('going to generate image!')
    print('generation_params: ', generation_params)
    heatmap_description = json.loads(generation_params['heatmap_description'])

    img_generator = ImageGenerator(heatmap_description=heatmap_description, server_conn=server_conn,
                                   output_dir=output_dir)
    img_generator.generate_image()

    # print('server_conn: ', server_conn)
    # print('output_dir: ', output_dir)


class ImageGenerator:
    """
    Class that produces an image that summarises the heatmap
    """

    def __init__(self, heatmap_description, server_conn, output_dir):
        """
        Create an instance of the class that will process the heatmap
        :param heatmap_description: description of the heatmap
        :param server_conn: connection to the delayed jobs server to update progress
        :param output_dir: directory where to write the final image
        """
        self.heatmap_description = heatmap_description
        self.x_axis_entity_id = heatmap_description["xAxis"]["entityID"]
        self.x_axis_index_name = entities.get_index_name(self.x_axis_entity_id)
        self.x_axis_query = get_axis_ids_query(heatmap_description, "xAxis")
        self.x_axis_id_property = get_id_property_for_cell_join(self.x_axis_entity_id)

        self.y_axis_entity_id = heatmap_description["yAxis"]["entityID"]
        self.y_axis_index_name = entities.get_index_name(self.y_axis_entity_id)
        self.y_axis_query = get_axis_ids_query(heatmap_description, "yAxis")
        self.y_axis_id_property = get_id_property_for_cell_join(self.y_axis_entity_id)

        self.cells_index_name = 'chembl_eubopen_activity'
        self.all_cells_data_filepath = 'heatmap_photo/data/all_cell_data.json'
        self.all_cells_query = self.get_all_cells_query()
        self.properties_to_paint = ['num_activities', 'fake_property']
        # self.properties_to_paint = ['fake_property']

        self.axis_page_size = 1000
        self.quadrant_side_length = 100
        self.scan_size = 1000
        self.num_random_cell_checks = 100
        self.es_conn = es_connection.get_es_connection_from_env_config()
        self.server_conn = server_conn
        self.output_dir = output_dir

        self.all_x_ids_from_axis = set()
        self.all_y_ids_from_axis = set()
        self.all_x_ids_from_cells = set()
        self.all_y_ids_from_cells = set()

    def generate_image(self):
        """
        generates the image
        :return: full path to the generated image
        """
        all_x_ids = self.load_all_axis_ids(True)
        num_x_chunks = math.ceil(len(all_x_ids) / self.quadrant_side_length)

        self.all_x_ids_from_axis = set(all_x_ids)

        all_y_ids = self.load_all_axis_ids(False)
        num_y_chunks = math.ceil(len(all_y_ids) / self.quadrant_side_length)
        num_quadrants = num_x_chunks * num_y_chunks

        self.all_y_ids_from_axis = set(all_y_ids)

        print('num_quadrants: ', num_quadrants)
        num_quadrants_read = 0
        num_cells_read = 0

        all_cells_data_dict = self.load_all_cells_data_dict()
        props_ranges = self.get_properties_range(all_cells_data_dict)
        print('props_ranges: ', props_ranges)
        self.save_intermediary_step(all_x_ids, 'all_x_ids')
        self.save_intermediary_step(all_y_ids, 'all_y_ids')

        for prop_id in self.properties_to_paint:
            print('prop_id: ', prop_id)
            raw_matrix_values = get_raw_matrix_values(prop_id, all_y_ids, all_x_ids,
                                                      all_cells_data_dict)

            self.save_intermediary_step(raw_matrix_values, f'raw_matrix_values-{prop_id}')
            self.check_random_cells(all_x_ids, all_y_ids, raw_matrix_values, prop_id)
            colours_array = self.get_colours_array(raw_matrix_values, prop_id, props_ranges[prop_id])
            self.save_intermediary_step(colours_array, 'colours_array')
            print('---')

    def save_intermediary_step(self, raw_data, step_name):
        """
        :param raw_data: raw data to save
        :param step_name: step name to output
        """
        output_filename = os.path.join(self.output_dir, 'intermediate_data', f'{step_name}.json')
        os.makedirs(os.path.dirname(output_filename), exist_ok=True)
        dict_to_save = {
            step_name: raw_data
        }
        with open(output_filename, 'w') as out_file:
            json.dump(dict_to_save, out_file)

    def check_random_cells(self, all_x_ids, all_y_ids, raw_matrix_values, prop_id):
        """
        checks randomnly self.num_random_cell_checks to see if their data is correct
        :param all_x_ids: all ids in the x-axis
        :param all_y_ids: all ids in the y-axis
        :param raw_matrix_values: all values of the cells obtained
        :param prop_id: property id being analysed.
        """

        pairs_to_check, all_x_ids_to_check, all_y_ids_to_check = self.get_pairs_to_check(all_x_ids, all_y_ids,
                                                                                         raw_matrix_values)
        values_must_be = self.get_check_values_must_be(all_x_ids_to_check, all_y_ids_to_check)
        for item in pairs_to_check:
            x_id = item['x_id']
            y_id = item['y_id']
            value_to_check = item['value_to_check']
            default_value = 0 if prop_id == 'num_activities' else None
            value_must_be = values_must_be.get(y_id, {}).get(x_id, {}).get(prop_id, default_value)
            assert value_to_check == value_must_be, \
                f'The value of {prop_id} seems wrong for x:{x_id},y:{y_id}, value_to_check: {value_to_check}' \
                f', value_must_be:{value_must_be}'



    def get_pairs_to_check(self, all_x_ids, all_y_ids, raw_matrix_values):
        """
        :param all_x_ids: all the ids in the x-axis
        :param all_y_ids: all ids in the  y-axis
        :param raw_matrix_values: raw values of the cells
        :return: the random pairs to check, as well as all the x and y ids to check
        """

        i = 0
        pairs_to_check = []
        all_x_ids_to_check = []
        all_y_ids_to_check = []
        while i < self.num_random_cell_checks:
            x_index = random.randint(0, len(all_x_ids) - 1)
            x_id = all_x_ids[x_index]
            all_x_ids_to_check.append(x_id)
            y_index = random.randint(0, len(all_x_ids) - 1)
            y_id = all_y_ids[y_index]
            all_y_ids_to_check.append(y_id)

            pairs_to_check.append({
                'x_index': x_index,
                'y_index': y_index,
                'x_id': x_id,
                'y_id': y_id,
                'value_to_check': raw_matrix_values[y_index][x_index]
            })
            i += 1

        return pairs_to_check, all_x_ids_to_check, all_y_ids_to_check

    def get_check_values_must_be(self, all_x_ids_to_check, all_y_ids_to_check):
        """
        :param all_x_ids_to_check: ids to check for the x-axis
        :param all_y_ids_to_check: ids to check for the y-axis
        :return: a dict with what the values must be
        """

        query = self.get_specific_cells_query(all_x_ids_to_check, all_y_ids_to_check)
        print('cells_index_name: ')
        print(self.cells_index_name)
        print('query: ')
        print(json.dumps(query))
        values_must_be = {}
        for doc_i in scan(self.es_conn, index=self.cells_index_name, query=query,
                          size=self.scan_size):

            doc_source = doc_i['_source']
            y_id = doc_source[self.y_axis_id_property]
            x_id = doc_source[self.x_axis_id_property]

            if values_must_be.get(y_id) is None:
                values_must_be[y_id] = {}
            if values_must_be[y_id].get(x_id) is None:
                values_must_be[y_id][x_id] = {
                    'num_activities': 0,
                }
            values_must_be[y_id][x_id]['num_activities'] += 1
            values_must_be[y_id][x_id]['fake_property'] = get_fake_property_value(x_id, y_id)

        return values_must_be

    def get_properties_range(self, all_cells_data_dict, ):
        """
        :param all_cells_data_dict: dictionary with all cells' data
        :return: a dict with the maximum and minimum of the properties that will be painted
        """
        props_ranges = {}
        for prop_id in self.properties_to_paint:
            min_value = sys.maxsize
            max_value = -sys.maxsize - 1
            for row_data in all_cells_data_dict.values():
                for cell_data in row_data.values():
                    prop_value = cell_data[prop_id]
                    if prop_value is None:
                        continue
                    if prop_value > max_value:
                        max_value = prop_value
                    if prop_value < min_value:
                        min_value = prop_value
            props_ranges[prop_id] = {
                'min': min_value,
                'max': max_value
            }

        return props_ranges

    def get_colours_array(self, raw_matrix_values, prop_id, prop_range):
        """
        :param raw_matrix_values: raw values for the property used
        :param prop_id: id of the property that is being used
        :param prop_range: ranges (min and max values) for the property to paint
        :return: a numpy array with the corresponding colors for each value
        """
        # print('get_colours_array:')
        #
        # print('raw_matrix_values: ', raw_matrix_values)

        matrix_array = np.array(raw_matrix_values)
        if prop_id == 'num_activities':
            matrix_array = np.nan_to_num(matrix_array, nan=0)

        print(matrix_array[:5])
        # print(matrix_array.shape)
        not_none_positions = np.where(matrix_array is not None)
        # print('not_none_positions: ', not_none_positions)
        #
        # print('prop_range:', prop_range)

        self.server_conn.update_job_progress(10, f'Matrix Loaded. Shape is  {matrix_array.shape}')



    def load_all_axis_ids(self, is_x):
        """
        :is_x: true if x-axis, false if y-axis
        :return: all ids in the x-axis
        """
        index_name = self.x_axis_index_name if is_x else self.y_axis_index_name
        full_query = self.x_axis_query if is_x else self.y_axis_query
        id_property = self.x_axis_id_property if is_x else self.y_axis_id_property

        all_ids = []

        for doc_i in scan(self.es_conn, index=index_name, query=full_query, preserve_order=True,
                          size=self.scan_size):
            doc_source = doc_i['_source']
            item_id = doc_source[id_property]
            all_ids.append(item_id)

        return all_ids

    def load_all_cells_data_dict(self):
        """
        :return: a dictionary with all the data in the cells
        """
        if os.path.exists(self.all_cells_data_filepath):
            self.server_conn.update_job_progress(0,
                                                 f'Found file {self.all_cells_data_filepath} '
                                                 f'({os.path.getsize(self.all_cells_data_filepath)} B). Loading...')

            with open(self.all_cells_data_filepath, 'r') as in_file:
                all_cells_data_dict = json.load(in_file)

            self.server_conn.update_job_progress(10,
                                                 f'{self.all_cells_data_filepath} loaded!')
        else:
            self.server_conn.update_job_progress(0,
                                                 f'NOT found file {self.all_cells_data_filepath} I will generate it...')
            all_cells_data_dict = self.get_all_cells_data_dict()

            with open(self.all_cells_data_filepath, 'w') as out_file:
                json.dump(all_cells_data_dict, out_file)

            self.server_conn.update_job_progress(10,
                                                 f'{self.all_cells_data_filepath} '
                                                 f'({os.path.getsize(self.all_cells_data_filepath)} B). generated!')

        return all_cells_data_dict

    def get_all_cells_data_dict(self):
        """
        :return: the data available for all the cells
        """
        all_data = {}

        for doc_i in scan(self.es_conn, index=self.cells_index_name, query=self.all_cells_query,
                          size=self.scan_size):

            doc_source = doc_i['_source']
            y_id = doc_source[self.y_axis_id_property]
            x_id = doc_source[self.x_axis_id_property]

            self.all_x_ids_from_cells.add(x_id)
            self.all_y_ids_from_cells.add(y_id)

            if all_data.get(y_id) is None:
                all_data[y_id] = {}
            if all_data[y_id].get(x_id) is None:
                all_data[y_id][x_id] = {
                    'num_activities': 0,
                }
            all_data[y_id][x_id]['num_activities'] += 1
            all_data[y_id][x_id]['fake_property'] = get_fake_property_value(x_id, y_id)

        return all_data

    def get_all_cells_query(self):
        """
        :return: a query to get all the cell data at once
        """
        return {
            "_source": [
                f'{self.x_axis_id_property}',
                f'{self.y_axis_id_property}'
            ],
            "sort": [
                {"_id": "desc"}  # this sort is used to be able to use the search_after parameter
            ]
        }

    def get_specific_cells_query(self, x_ids, y_ids):
        """
        :param x_ids: x ids to get
        :param y_ids: y ids to get
        :return: a query to get all the cell data at once
        """

        return {
            "_source": [
                f'{self.x_axis_id_property}',
                f'{self.y_axis_id_property}'
            ],
            "query": {
                "bool": {
                    "filter": [
                        {
                            "terms": {
                                f'{self.x_axis_id_property}': x_ids
                            }
                        },
                        {
                            "terms": {
                                f'{self.y_axis_id_property}': y_ids
                            }
                        }
                    ],

                }
            },
            "sort": [
                {"_id": "desc"}  # this sort is used to be able to use the search_after parameter
            ]
        }


def get_axis_ids_query(heatmap_description, axis):
    """
    :param heatmap_description: description of the heatmap
    :param axis: axis in the heatmap description to use xAxis or yAxis
    :return: the query to use to get all the ids of an axis
    """
    axis_query = heatmap_description[axis]["initialQuery"]
    axis_entity_id = heatmap_description[axis]["entityID"]
    id_property = get_id_property_for_cell_join(axis_entity_id)
    axis_query['_source'] = id_property
    return axis_query


def get_id_property_for_cell_join(entity_id):
    """
    :param entity_id: The entity ID of the axis
    :return: the property to be used to join the axis with the cells
    """
    if entity_id == entities.EntityIDs.EUBOPEN_COMPOUND.value:
        return 'molecule_chembl_id'

    if entity_id == entities.EntityIDs.EUBOPEN_TARGET.value:
        return 'target_chembl_id'


def get_fake_property_value(x_item, y_item):
    """
    :param x_item: id of the x item
    :param y_item: id of the y item
    :return: a value representing a fake property from 0 to 100, it could also be an undefined value
    """
    raw_value = FarmHash128(f'{x_item}-{y_item}') % 100
    value = None if raw_value < 20 else raw_value
    return value


def get_raw_matrix_values(prop_id, all_y_ids, all_x_ids, all_cells_data_dict):
    """
    :param prop_id: id of the property to get from all the data
    :param all_y_ids: all y ids in the required order
    :param all_x_ids: all y ides in the required order
    :param all_cells_data_dict: dictionary with all the data of the cells
    :return: all  values for the given property as an aray of arrays
    """

    raw_matrix_values = []

    i = 0
    for y_id in all_y_ids:

        row_values = []
        raw_row_data = all_cells_data_dict.get(y_id, {})

        j = 0
        for x_id in all_x_ids:
            cell_data = raw_row_data.get(x_id, {})
            default_value = 0 if prop_id == 'num_activities' else None
            cell_value_prop = cell_data.get(prop_id, default_value)

            row_values.append(cell_value_prop)
            j += 1

        raw_matrix_values.append(row_values)
        i += 1

    return raw_matrix_values

# def get_color(prop_range, prop_value):
#     """
#     :param prop_range:
#     :param prop_value:
#     :return:
#     """
#
#          blues = plt.get_cmap('Blues')
#          return mcolors.to_hex(blues(t))

# import numpy as np
#
# # Define the 2D array
# arr = np.array([[1, 2, 3],
#                 [4, 5, 6],
#                 [7, 8, 9]])
#
# # Define the vectorized function
# vectorized_fn = np.vectorize(interpolate_blues)
#
# # Apply the function to the array
# color_arr = vectorized_fn(arr)
#
# # Print the resulting color array
# print(color_arr)
#
#
#
# def colour(matrix):
#
#
#
#     def interpolate_blues(t):
#         blues = plt.get_cmap('Blues')
#         return mcolors.to_hex(blues(t))
