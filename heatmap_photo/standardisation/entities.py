"""
Module that contains the standardisation for the entities names, ids and assigned indexes
"""
from enum import Enum


class EntityIDs(Enum):
    """
    Enumeration with the possible entity ids
    """
    EUBOPEN_COMPOUND = 'EubopenCompound'
    EUBOPEN_TARGET = 'EubopenTarget'


INDEX_NAMES = {
    EntityIDs.EUBOPEN_COMPOUND: 'chembl_eubopen_molecule',
    EntityIDs.EUBOPEN_TARGET: 'chembl_eubopen_target'
}


def get_index_name(entity_id):
    """
    :param entity_id: the entity id to query
    :return: the index name corresponding to the entity id
    """
    return INDEX_NAMES.get(EntityIDs(entity_id))
