#!/usr/bin/env bash

set -x
set -e

source /app/python_env/bin/activate
PYTHONPATH=/app/python_env/lib/python3.9/site-packages/:/app python3 /app/heatmap_photo/run_job.py $1
deactivate
